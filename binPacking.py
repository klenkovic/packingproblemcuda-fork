#First-fit algoritam - Rjesavanje problema pakiranja

import math
import sys
import binModule
import numpy as np

print "\n\n"

# Korisnik unosi kapacitet spremnika
cap = binModule.getCap()

# Korisnik unosi elemente
items = binModule.getItems()

#inicijalizacija polja
bins = np.zeros((2, len(items)), dtype=np.float32)

print "Vasi elementi su:", items, "\nVasi spremnici imaju kapacitet ", cap, "\n"

#cijeli prvi redak polja postaje jednak kapacitetu
for x in range(0, len(items)):
        bins[0][x] = cap

print bins

#maximalni dosegnuti spremnik polja biti ce u y
y = 0
for item in items:
        x = 0
        # Dodavanje elemenata u prvi spremnik koji ih moze primiti
        # Ukoliko ih niti jedan spremnik ne moze prihvatiti, napravi novi spremnik
	
	#ako je element veci od kapaciteta - error!     
	if item > cap:
                print "Element ", item, "je veci od kapaciteta spremnika koji je ", cap,". PREKID!"
                sys.exit()
	#provjera dali se moze element smjestiti te ako moze smjesta se
        if bins[0][x] >= item:
                bins[1][x] += item
                bins[0][x] -= item
        else:
		#ako se nemoze smjestiti ide se do prvog spremnika u koji se moze
		while bins[0][x] < item:
	                x+=1
                bins[1][x] += item
                bins[0][x] -= item
	#provjera dali je trenutni x > y, ako je to je y jer je to max dosegnuti spremnik
	if x > y:
		y = x

print "First-fit algoritam za", items, "sa kapacitetom", cap, "koristio je", y+1, "spremika"
print "Konfiguracija: ", bins
