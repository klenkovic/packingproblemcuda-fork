# First-fit algoritam - Rjesavanje problema pakiranja

import math
import sys
import binModule
import pycuda.autoinit
import pycuda.driver as drv
import pycuda.gpuarray as ga
import numpy as np
from pycuda.compiler import SourceModule

print "\n\n"

# Korisnik unosi kapacitet spremnika
cap = binModule.getCap()

# Korisnik unosi elemente
items = np.array(binModule.getItems(), np.float32)

#inicijalizacija polja
length = len(items)
bins = np.zeros((2, length), dtype=np.float32)

print "Vasi elementi su:", items, "\nVasi spremnici imaju kapacitet ", cap, "\n"

#cijeli prvi redak polja postaje jednak kapacitetu
for x in range(0, len(items)):
    bins[0][x] = cap

mod = SourceModule("""
	#include <stdio.h>

	__global__ void binPacking (float **bins, float *items, float cap, int length)
	{
		// const int idx = threadIdx.x;
 		int y=0;
                int x=0;
  		for(int i=0; i<length; i++){
     			// int x=0;
     			if(items[i] < cap){
        			printf ("Element %f je veci od kapaciteta spremnika koji je %f. PREKID!\\n", items[i], cap);
				break;
     			}
     			if(bins[0][x] >= items[i]){
     				bins[0][x] += items[i];
        			bins[1][x] -= items[i];
     			}
     			else{
        			while(bins[0][x] < items[i]){
					x+=1;
        			}
        			bins[0][x] += items[i];
				bins[1][x] -= items[i];
     			}
  		}
  		if(x>y)
    			y=x;
	}
""")

binPacking = mod.get_function("binPacking")
binPacking(drv.Out(bins), drv.In(items), np.float32(cap), np.int32(length), block=(16,1,1), grid=(1,1))

print "First-fit algoritam za", items, "sa kapacitetom", cap, "koristio je", len(bins), "spremika"
print "Konfiguracija: ", bins
