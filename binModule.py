def getCap():
	while True:
		try:
			cap = float(raw_input('Unesite kapacitet spremnika (npr: 5): ').strip())
			break
		except ValueError:
			print "Nazalost Vas unos nije valjan!"
	return cap

# Unos elemenata od strane korisnika
def getItems():
	while True:
		try:
			items = [float(n) for n in raw_input('Unesite tezine elemenata, razdvojite razmakom (npr: 2 4 3): ').strip().split(' ')]
			break
		except ValueError:
			print "Nazalost Vas unos nije valjan!"	
	return items